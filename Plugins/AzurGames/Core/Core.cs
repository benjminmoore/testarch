﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class Contract : IDisposable {
    public readonly List<Contract> SubContracts = new List<Contract>();

    public GameObject Target { get; protected set; }

    protected virtual void Initialize() {
    }

    protected void PostInitialize() {
        this.SubContracts
            .ForEach(c => {
                c.Initialize();
                c.PostInitialize();
            });
    }

    public virtual void Dispose() {
        this.SubContracts.ForEach(c => c.Dispose());
        this.SubContracts.Clear();
    }
}

public abstract class Contract<T0> : Contract where T0 : Contract<T0>, new() {
    private static readonly List<T0> Contracts = new List<T0>();

    public static T0 Get(Component component) {
        if (component == null) throw new ArgumentNullException("component");
        return Get(component.gameObject);
    }

    public static T0 Get(GameObject target) {
        if (target == null) throw new ArgumentNullException("target");


        var contract = TryGet(target);
        if (contract == null) {
            contract = new T0 {
                Target = target
            };
            contract.Initialize();
            contract.PostInitialize();
            Contracts.Add(contract);
        }
        return contract;
    }

    public static T0 TryGet(Component component) {
        if (component == null) throw new ArgumentNullException("component");
        return TryGet(component.gameObject);
    }

    public static T0 TryGet(GameObject target) {
        if (target == null) throw new ArgumentNullException("target");
        return Contracts.FirstOrDefault(c => c.Target != null && c.Target == target);
    }

    protected T1 GetSub<T1>() where T1 : Contract<T1>, new() {
        var local = Contract<T1>.Get(this.Target);
        this.SubContracts.Add(local);
        return local;
    }

    public override void Dispose() {
        Contracts.Remove((T0) this);
        base.Dispose();
    }
}

public sealed class Container : IDisposable {
    private readonly Dictionary<Type, Contract> contracts = new Dictionary<Type, Contract>();

    public void Register<T>(T contract) where T : Contract {
        if (contract == null) throw new ArgumentNullException("contract");
        this.Register(contract, typeof(T));
    }

    private void Register(Contract contract, Type type) {
        this.RegisterLocal(contract, type);
        contract.SubContracts.ForEach(this.Register);
    }

    private void RegisterLocal(Contract contract, Type type) {
        if (!this.contracts.ContainsKey(type) && !this.contracts.ContainsValue(contract)) {
            this.contracts.Add(type, contract);
        }
    }

    public void Unregister<T>(T contract) where T : Contract {
        if (contract == null) throw new ArgumentNullException("contract");
        if (this.contracts.ContainsKey(typeof(T))) {
            this.contracts.Remove(typeof(T));
        }
    }

    public T Resolve<T>() where T : Contract {
        Contract contract;
        if (this.contracts.TryGetValue(typeof(T), out contract)) {
            return (T) contract;
        }
        return (T) contract;
    }

    public void Dispose() {
        this.contracts.Clear();
    }
}

public static class ContractExtension {
    public static T TryGet<T>(this Component obj) where T : Contract<T>, new() {
        return Contract<T>.TryGet(obj);
    }

    public static T TryGet<T>(this GameObject obj) where T : Contract<T>, new() {
        return Contract<T>.TryGet(obj);
    }

    public static T Get<T>(this Component obj) where T : Contract<T>, new() {
        return Contract<T>.Get(obj);
    }

    public static T Get<T>(this GameObject obj) where T : Contract<T>, new() {
        return Contract<T>.Get(obj);
    }
}

namespace UniRx {
    public static class Extensions {
        public static T0 AddTo<T0>(this T0 disposable, Contract contract) where T0 : IDisposable {
            if (contract == null) {
                disposable.Dispose();
                return disposable;
            }

            disposable.AddTo(contract.Target);
            return disposable;
        }
    }
}